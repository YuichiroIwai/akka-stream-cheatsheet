import java.util.concurrent.CompletableFuture
import java.util.function.Supplier

import akka.Done
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Broadcast, GraphDSL, Keep, Merge, Partition, RunnableGraph, Sink, Source, Unzip, UnzipWith}
import akka.stream.testkit.scaladsl.{TestSink, TestSource}
import akka.stream.{ActorMaterializer, ClosedShape}

import scala.concurrent.Promise
import scala.concurrent.duration._
import scala.io.StdIn

object Main extends App {
  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()

  val sourceStages = new SourceStages
  val sinkStages = new SinkStages
  val simpleProcessingStages = new SimpleProcessingStages()
  val fanInStages = new FanInStages
  val fanOutStages = new FanOutStages

  {
    import sourceStages._
    fromIterator()
    apply()
    single()
    repeat()
    cycle()
    tick()
    fromFuture()
    fromCompletionStage()
    unfold()
  }

  {
    import sinkStages._
    head()
    headOption()
    last()
    lastOption()
    ignore()
    cancelled()
  }

  {
    import simpleProcessingStages._
    map()
    mapConcat()
    statefulMapConcat()
  }

  {
    import fanInStages._
    merge()
  }

  {
    import fanOutStages._
    unzip()
    unzipWith()
    broadcast()
  }

  StdIn.readLine()
  system.terminate()
}

class SourceStages()(implicit system: ActorSystem, materializer: ActorMaterializer) {
  /**
    * Source.fromIterator[T](f: () => Iterator[T]): Source[T, NotUsed]
    *
    * Iterator[T]を返す関数を渡し、Source[T, NotUsed]を取得
    * Iterator[T]を直接渡さない点に注意
    */
  def fromIterator(): Unit = {
    Source.fromIterator(() => (1 to 3).toIterator)
      .runWith(TestSink.probe)
      .request(3)
      .expectNext(1, 2, 3)
      .expectComplete()
  }

  /**
    * Source.apply[T](Iterable[T]): Source[T, NotUsed]
    *
    * Iterable[T]を渡してSource[T, NotUsed]を取得
    * Source(...)と書けるので、一番シンプル
    */
  def apply(): Unit = {
    Source(1 to 3)
      .runWith(TestSink.probe)
      .request(3)
      .expectNext(1, 2, 3)
      .expectComplete()
  }

  /**
    * Source.single[T](element: T): Source[T, NotUsed]
    *
    * 指定した要素を一つだけ下流に流して終了する
    */
  def single(): Unit = {
    Source.single(10)
      .runWith(TestSink.probe)
      .request(1)
      .expectNext(10)
      .expectComplete()
  }

  /**
    * Source.repeat[T](element: T): Source[T, NotUsed]
    *
    * 指定した要素を下流に流し続ける
    */
  def repeat(): Unit = {
    Source.repeat(10)
      .runWith(TestSink.probe)
      .request(5)
      .expectNext(10, 10, 10, 10, 10)
      .cancel()
  }

  /**
    * Source.cycle[T](f: () => Iterator[T]): Source[T, NotUsed]
    *
    * Iterator[T]を返す関数を渡して繰り返し下流へ流すSource[T, NotUsed]を取得する
    */
  def cycle(): Unit = {
    Source.cycle(() => (1 to 3).toIterator)
      .runWith(TestSink.probe)
      .request(10)
      .expectNext(1, 2, 3, 1, 2, 3, 1, 2, 3, 1)
      .cancel()
  }

  /**
    * Source.tick[T](initialDelay: FiniteDuration, interval: FiniteDuration, tick: T): Source[T, Cancellable]
    *
    * 開始までの時間と、開始後のインターバルを指定し、一定間隔で下流にT型の要素を流し続ける
    * SourceがCancellableにマテリアライズされるので、これを利用してキャンセルが可能
    * 下流が詰まっているときはスキップされる
    */
  def tick(): Unit = {
    Source.tick(0.second, 1.second, "test")
      .runWith(TestSink.probe)
      .request(2)
      .expectNext("test", "test")
      .cancel()

    // キャンセルしてみる
    val (cancellable, probe) = Source.tick(0.second, 1.second, "test")
      .toMat(TestSink.probe)(Keep.both)
      .run()
    assert(cancellable.cancel())
    probe.request(1)
      .expectComplete()
  }

  /**
    * Source.fromFuture[T](future: Future[T]): Source[T, NotUsed]
    *
    * Futureを渡してSource[T, NotUsed]を取得する
    * 渡したFutureの完了時に結果を下流に流す
    */
  def fromFuture(): Unit = {
    val p1 = Promise[Int]
    val probe = Source.fromFuture(p1.future)
      .runWith(TestSink.probe)

    p1.success(10)
    probe
      .request(1)
      .expectNext(10)
      .expectComplete()
  }

  /**
    * Source.fromCompletionStage[T](future: CompletionStage[T]): Source[T, NotUsed]
    *
    * JavaのCompletionStage[T]を渡してSource[T, NotUsed]を得る
    */
  def fromCompletionStage(): Unit = {
    Source.fromCompletionStage(CompletableFuture.supplyAsync(new Supplier[Int] {
      override def get(): Int = 1
    }))
      .runWith(TestSink.probe)
      .request(1)
      .expectNext(1)
      .expectComplete()
  }

  /**
    * Source.unfold[S, E](zero: S)(f: S => Option[(S, E)]): Source[E, NotUsed]
    *
    * 初期値と関数の2つの引数を取る
    * 関数には、前の実行時の結果が引数として渡される
    * Noneを返すと終了、Someを返すと引き続き処理が続行される
    * Someでは、次の実行時の引数と、現在の実行時の結果のタプルとして返す
    */
  def unfold(): Unit = {
    Source.unfold(1) { i => if (i > 5) None else Some(i + 1 -> i) }
      .runWith(TestSink.probe)
      .request(6)
      .expectNext(1, 2, 3, 4, 5)
      .expectComplete()
  }
}

class SinkStages()(implicit system: ActorSystem, materializer: ActorMaterializer) {

  import system.dispatcher

  /**
    *  Sink.head[T]: Sink[T, Future[T]]
    *
    * 上流から要素を一つ取得してFuture[T]でマテリアライズする
    * 要素を一つ読み取った時点でcancelされる
    * 上流から要素が一つも流れてこない場合はFutureは失敗する
    */
  def head(): Unit = {
    val (probe, future) = TestSource.probe[Int]
      .toMat(Sink.head)(Keep.both)
      .run()
    future.foreach(i => assert(i == 1))
    probe
      .sendNext(1)
      .expectCancellation()
  }

  /**
    * Sink.headOption[T]: Sink[T, Future[Option[T]]]
    *
    * 上流から要素を一つ取得してFuture[Option[T]]でマテリアライズする
    * 要素を一つ読み取った時点でFuture[Some[T]]
    * 上流から要素が一つも流れてこない場合はFuture[None]になる
    */
  def headOption(): Unit = {
    val (probe1, future1) = TestSource.probe[Int]
      .toMat(Sink.headOption)(Keep.both)
      .run()
    future1.foreach {
      case Some(i) => assert(i == 1)
      case None => sys.error("expected Some, but None given")
    }
    probe1
      .sendNext(1)
      .expectCancellation()

    val (probe2, future2) = TestSource.probe[Int]
      .toMat(Sink.headOption)(Keep.both)
      .run()
    future2.foreach {
      case Some(_) => sys.error("expected None, but Some given")
      case None =>
    }
    probe2
      .sendComplete()
  }

  /**
    * Sink.last[T]: Sink[T, Future[T]]
    *
    * 上流の最後の要素をFuture[T]でマテリアライズする
    * 上流から要素が流れてこなかった場合はFutureは失敗する
    */
  def last(): Unit = {
    val (probe, future) = TestSource.probe[Int]
      .toMat(Sink.last)(Keep.both)
      .run()
    future foreach (i => assert(i == 3))
    probe
      .sendNext(1)
      .sendNext(2)
      .sendNext(3)
      .sendComplete()
  }

  /**
    * Sink.lastOption[T]: Sink[T, Future[Option[T]]
    *
    * lastのOption版
    * 上流から要素が流れてこなかった場合にはNoneが返る
    */
  def lastOption(): Unit = {
    val (probe, future) = TestSource.probe[Int]
      .toMat(Sink.lastOption)(Keep.both)
      .run()
    future foreach (i => assert(i.get == 3))
    probe
      .sendNext(1)
      .sendNext(2)
      .sendNext(3)
      .sendComplete()
  }

  /**
    * Sink.ignore[T]: Sink[T, Future[Done]]
    *
    * 流れてきた要素を無視する
    * 結果はFuture[Done]としてマテリアライズされる
    */
  def ignore(): Unit = {
    val (probe, future) = TestSource.probe[Int]
      .toMat(Sink.ignore)(Keep.both)
      .run()
    future foreach (done => assert(done.isInstanceOf[Done]))
    probe
      .sendNext(1)
      .sendNext(2)
      .sendNext(3)
      .sendComplete()
  }

  /**
    * Sink.cancelled[T]: Sink[T, NotUsed]
    *
    * 即座にキャンセルする
    * 上流からは要素を流すことができない
    */
  def cancelled(): Unit = {
    val probe = TestSource.probe[Int]
      .to(Sink.cancelled)
      .run()
    probe
      .expectCancellation()
  }
}

class SimpleProcessingStages()(implicit system: ActorSystem, materializer: ActorMaterializer) {
  /**
    * FlowOps[Out].map[T](f: Out => T): Repr[T]
    *
    * 各要素に関数fを適用した結果を下流へ流す
    */
  def map(): Unit = {
    Source(1 :: 2 :: 3 :: Nil)
      .map(_ * 2)
      .runWith(TestSink.probe)
      .request(3)
      .expectNext(2, 4, 6)
      .expectComplete()
  }

  /**
    * FlowOps[Out].mapConcat[T](f: Out => Iterable[T]): Repr[T]
    *
    * 要素に対して関数fを適用した結果の各要素をそれぞれ下流に流す
    */
  def mapConcat(): Unit = {
    Source.single(1)
      .mapConcat(i => i :: i + 1 :: i + 2 :: Nil)
      .runWith(TestSink.probe)
      .request(3)
      .expectNext(1, 2, 3)
      .expectComplete()
  }

  /**
    * FlowOps[Out].statefulMapConcat[T](f: () => (Out) => Iterable[T]): Repr[T]
    *
    * 基本的な考え方はmapConcatと同じだが、要素ごとに関数fが実行されるため、内部に状態を持てる
    */
  def statefulMapConcat(): Unit = {
    Source(1 :: 2 :: 3 :: Nil)
      .statefulMapConcat { () =>
        var count = 0
        (i) =>
          count = count + 1
          i * count :: Nil
      }.runWith(TestSink.probe)
      .request(3)
      .expectNext(1, 4, 9)
      .expectComplete()
  }
}

class FanInStages()(implicit system: ActorSystem, materializer: ActorMaterializer) {
  /**
    * Merge
    *
    * 複数の入力を受け取り、一つを選択して下流に流す
    */
  def merge(): Unit = {
    RunnableGraph.fromGraph(GraphDSL.create(TestSink.probe[Int]) { implicit builder =>
      s =>
        import GraphDSL.Implicits._

        val in = Source(1 :: 2 :: 3 :: Nil)
        val p = builder.add(Partition[Int](2, _ % 2))
        val m = builder.add(Merge[Int](2))

        in ~> p ~> m ~> s
        p ~> m

        ClosedShape
    })
      .run()
      .request(3)
      .expectNext(1, 2, 3)
      .expectComplete()
  }
}

class FanOutStages()(implicit system: ActorSystem, materializer: ActorMaterializer) {
  /**
    * Unzip[A, B]
    *
    * 1つの入力を受け、2つに分解して出力する
    * 入力は(A, B)のタプルで、出力はそれぞれAとBになる
    */
  def unzip(): Unit = {
    RunnableGraph.fromGraph(GraphDSL.create(TestSink.probe[Int]) { implicit builder =>
      s =>
        import GraphDSL.Implicits._

        val in = Source(for (i <- 1 to 3; j <- 4 to 5) yield (i, j))
        val uz = builder.add(Unzip[Int, Int])
        val m = builder.add(Merge[Int](2))

        in ~> uz.in
        uz.out0 ~> m ~> s
        uz.out1 ~> m
        ClosedShape
    })
      .run()
      .request(12)
      .expectNext(1, 4, 1, 5, 2, 4, 2, 5, 3, 4, 3, 5)
      .expectComplete()
  }

  /**
    * UnzipWith
    *
    * 1つの入力を任意の要素数Nのタプルに変換するための関数を与え、それぞれをN個に分岐して下流へと出力する
    */
  def unzipWith(): Unit = {
    RunnableGraph.fromGraph(GraphDSL.create(TestSink.probe[Int]) { implicit builder =>
      s =>
        import GraphDSL.Implicits._

        val in = Source(1 to 3)
        val uzw = builder.add(UnzipWith((i: Int) => Tuple3(i, i + 1, i + 2)))
        val m = builder.add(Merge[Int](3))

        in ~> uzw.in
        uzw.out0 ~> m ~> s
        uzw.out1 ~> m
        uzw.out2 ~> m

        ClosedShape
    })
      .run()
      .request(9)
      .expectNext(1, 2, 3, 2, 3, 4, 3, 4, 5)
      .expectComplete()
  }

  /**
    * Broadcast
    *
    * 1つの入力を任意のN個の下流に出力する
    */
  def broadcast(): Unit = {
    RunnableGraph.fromGraph(GraphDSL.create(TestSink.probe[Int]) { implicit builder =>
      s =>
        import GraphDSL.Implicits._

        val in = Source(1 to 3)
        val b = builder.add(Broadcast[Int](3))
        val m = builder.add(Merge[Int](3))

        in ~> b ~> m ~> s
        b ~> m
        b ~> m

        ClosedShape
    })
      .run()
      .request(9)
      .expectNext(1, 1, 1, 2, 2, 2, 3, 3, 3)
      .expectComplete()
  }
}
